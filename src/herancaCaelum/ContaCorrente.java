package herancaCaelum;

public class ContaCorrente extends Conta{
	public void atualiza (Double taxa) {
		this.saldo += this.saldo * taxa * 2;
	}
	
	public void deposita (Double valor) {
		this.saldo += valor - 0.10;
	}
}
