package herancaCaelum;

import java.io.IOException;

public class TestaContas {
	public static void main (String [] args) throws IOException{
		Conta c = new Conta();
		Conta cc = new ContaCorrente();
		Conta cp = new ContaPoupanca();
		
		Double valor = 1000.0;
		Double taxa = 0.01;
		
		c.deposita(valor);
		cc.deposita(valor);
		cp.deposita(valor);
		
		c.atualiza(taxa);
		cc.atualiza(taxa);
		cp.atualiza(taxa);
		
		System.out.println(c.getSaldo());
		System.out.println(cc.getSaldo());
		System.out.println(cp.getSaldo());
	}
}
