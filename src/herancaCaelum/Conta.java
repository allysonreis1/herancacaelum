package herancaCaelum;

public class Conta {
	protected Double saldo = 0.0;

	public Double getSaldo() {
		return saldo;
	}
	
	public void deposita (Double valor) {
		this.saldo += valor;
	}
	
	public void saca (Double valor) {
		this.saldo -= valor;
	}
	
	public void atualiza (Double taxa) {
		this.saldo += this.saldo + taxa;
	}
}
